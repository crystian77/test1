import {database, docTypes} from '../../database';


export class HotelModelClass {
    getById (id) {
        return database.findOne({docType: docTypes.HOTEL, id: parseInt(id)});
    }
}


export const hotelModel = new HotelModelClass();
