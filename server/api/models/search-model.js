import {database, docTypes} from '../../database';


export class SearchModelClass {
	
	getByFilter (place, payload) {
    	//TODO V1.1.0 using payload and other faster table
        return database.find({docType: docTypes.HOTEL, place: place})
          .then(document => {
          	return {
          		place,
				from: payload.from || 'xx-xx-xxxx',
				to: payload.to || 'xx-xx-xxxx',
				guest: payload.guest || 2,
				hotels: document
			};
		  });
    }
}


export const searchModel = new SearchModelClass();
