import {database, docTypes} from '../../database';

export class ConfigModelClass {
	getConfig(){
	    return database.findOne({docType: docTypes.CONFIG});
    }
}

export const configModel = new ConfigModelClass();
