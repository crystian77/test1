import {hotelModel} from '../models';

class HotelControllerClass {
    getById (req, res) {
        return hotelModel.getById(req.params.id)
            .then(document => res.json(document || {}))
            .catch(error => res.json({error: error.message}));
    }
}

export const hotelController = new HotelControllerClass();
