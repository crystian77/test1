import {searchModel} from '../models';

class SearchControllerClass {
    getByFilter (req, res) {
        return searchModel.getByFilter(req.params.place, req.body)
            .then(document => res.json(document || {}))
            .catch(error => res.json({error: error.message}));
    }
}

export const searchController = new SearchControllerClass();
