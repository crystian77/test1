import {configModel} from '../models';

class ConfigControllerClass {
	getConfig(req, res) {
		return configModel.getConfig()
			.then(documents => res.json(documents))
			.catch(error => res.json({error: error.message}));
	}
}

export const configController = new ConfigControllerClass();
