import express from 'express';

import {configController, hotelController, searchController} from './controllers';

export const router = express.Router();

// API Routes
router.get('/config', configController.getConfig);
router.get('/hotels/:id', hotelController.getById);
router.get('/search/:place', searchController.getByFilter);
router.post('/search/:place', searchController.getByFilter);
