import fetch from 'node-fetch';

import {app} from '../';

const PORT = 5001;
let server;

export function callAPI(path) {
	// console.log('API Called:',path);
    return fetch(`http://localhost:${PORT}${path}`)
        .then(res => res)
        .catch(err => {
            throw err;
        });
}

export function startServer () {
    return new Promise((resolve, reject) => {
        server = app.listen(PORT, resolve);
        server.on('error', reject);
    });
}

export function stopServer () {
    return new Promise((resolve, reject) => {
        server.on('error', reject);
        server.close(resolve);
    });
}

export function restartServer () {
    return stopServer().then(startServer);
}
