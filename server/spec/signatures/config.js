export const configSignature = {
	'docType': jasmine.stringMatching('CONFIG'),
	'currencies': jasmine.any(Array),
	'servicesIcon': jasmine.any(Array),
	'servicesType': jasmine.any(Array)
};