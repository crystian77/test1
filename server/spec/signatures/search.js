export const searchSignature = {
	'place': jasmine.any(String),
	'from': jasmine.any(String),
	'to': jasmine.any(String),
	'guest': jasmine.any(Number),
	'hotels': jasmine.any(Array)
};
