export const hotelSignature = {
	'docType': jasmine.stringMatching('HOTEL'),
	'id': jasmine.any(Number),
	'template': jasmine.any(Number),
	'name': jasmine.any(String),
	'recommended': jasmine.any(Boolean),
	'place': jasmine.any(String),
	'priceOff': jasmine.any(Number),
	'pictures': jasmine.any(Array),
	'stars': jasmine.any(Number),
	'serviceType': jasmine.any(Number),
	'serviceIcons': jasmine.any(Array),
	'price': jasmine.any(Number),
	'currency': jasmine.any(Number),
	'destinationPay': jasmine.any(Boolean),
	'quotes': jasmine.any(Boolean)
};