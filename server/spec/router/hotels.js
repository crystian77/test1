import {callAPI} from '../utils';
import {hotelSignature} from '../signatures/hotel';

let res;
let json;

describe(`to /hotels/1`, () =>{
	beforeAll((done) =>{
		callAPI('/hotels/1')
		  .then(data => res = data)
		  .then(data => res.json())
		  .then(data => json = data)
		  .then(done, done.fail);
	});
	
	
	it(`should return a 200 status code`, () =>{
		expect(res.status).toBe(200);
	});
	
	it(`should return an object`, () =>{
		expect(json).toEqual(jasmine.any(Object));
	});
	
	// registers multiple tests
	registerDynamicsTest(hotelSignature);
});


function registerDynamicsTest(itemSignature){
	Object.keys(itemSignature).forEach(key =>{
		it(`should be an object containing '${key}'`, () =>{
			if(json instanceof Array){
				expect(json.length).toBeGreaterThan(0);
			} else {
				json = [json];
			}
			
			json.forEach(item =>{
				expect(item).toEqual(
				  jasmine.objectContaining({[key]: itemSignature[key]})
				);
			});
		});
	});
}
