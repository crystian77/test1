import {callAPI} from '../utils';
import {searchSignature} from '../signatures/search';

//TODO REMOVE DATABASE MOCK
//TODO looking the way to move
//TODO Add test by POST

//shame :$
let res;
let json;

describe(`to /search/:place`, () =>{
	beforeAll((done) =>{
		callAPI('/search/madrid')
		  .then(data => res = data)
		  .then(data => res.json())
		  .then(data => json = data)
		  .then(done, done.fail);
	});
	
	
	it(`should return a 200 status code`, () =>{
		expect(res.status).toBe(200);
	});
	
	it(`should return an array of hotels`, () =>{
		expect(json.hotels).toEqual(jasmine.any(Array));
	});
	
	it(`should be an object`, function () {
		expect(json.hotels.every(item => item instanceof Object)).toBe(true);
	});
	
	// registers multiple tests
	registerDynamicsTest(searchSignature);
});


function registerDynamicsTest(itemSignature){
	Object.keys(itemSignature).forEach(key =>{
		it(`should be an object containing '${key}'`, () =>{
			if(json instanceof Array){
				expect(json.length).toBeGreaterThan(0);
			} else {
				json = [json];
			}
			
			json.forEach(item =>{
				expect(item).toEqual(
				  jasmine.objectContaining({[key]: itemSignature[key]})
				);
			});
		});
	});
}
