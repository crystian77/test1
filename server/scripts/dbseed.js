import _ from 'lodash';
import colors from 'colors/safe';
import Promise from 'bluebird';
import {database} from '../database';
import fs from 'fs';
let fsQ = Promise.promisifyAll(fs);

const seedFolderPath = './database/seeds';

fsQ.readdirAsync(seedFolderPath)
  .then(fileNames =>{
	  return fileNames.map(fileName => seedFolderPath + '/' + fileName);
	  
  })
  .then(folderNames =>{
	  return Promise.map(folderNames, folderName => {
		  return fsQ.readdirAsync(folderName).map((folderChildren) => folderName +'/'+ folderChildren);
	  });
  })
  .then(file => {
	  Promise.map(_.flatten(_.concat(file)), f => {
		  return database.insert(require('.'+ f));
	  });
  })
  .catch(error =>{
	  console.log(colors.red('Error while reading seeds', error));
  });
