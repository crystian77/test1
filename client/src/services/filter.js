export default class FilterService {
	
    constructor(){
    }
	
	getRangesFromHotels(hotels = []){
    	let stars = [0,0,0,0,0],
			min, max;
    	
		hotels.forEach(hotel => {
			stars[hotel.stars-1]++;
			
			if(angular.isUndefined(min)){ min = hotel.price; }
			if(angular.isUndefined(max)){ max = hotel.price; }
			
			if(hotel.price < min) { min = hotel.price; }
			if(hotel.price > max) { max = hotel.price; }
			
		});
		
		// stars = _.reverse(stars);
		
		return {
			stars,
			min,
			max
		}
	}
}
