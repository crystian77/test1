import ApiService from './api';
import ConfigService from './config';
import FilterService from './filter';

let servicesModule = angular.module('servicesModule', [])
	.service(ApiService.name, ApiService)
	.service(ConfigService.name, ConfigService)
	.service(FilterService.name, FilterService);

export default servicesModule;

