export default class ApiService {
	//TODO move it to a property file
    url = 'http://localhost:5000';

    constructor($http){
        this.http = $http;
    }
    
    getConfig(){
		return this.http.get(`${this.url}/config`)
				.then(response => response.data);
	}
	
	search(place){
		return this.http.get(`${this.url}/search/${place}`)
				.then(response => response.data);
	}
	
	searchDetail(place, payload = {from: 'fromClient',to: 'toClient', guest: 2}){
		return this.http.post(`${this.url}/search/${place}`, payload)
				.then(response => response.data);
	}
	
	getHotelById(id){
		return this.http.get(`${this.url}/hotels/${id}`)
				.then(response => response.data);
	}
}
