import refineHotelsComponent from './refineHotels.component';

let refineHotelsModule = angular.module('refineHotelsModule', [])
	.component('refineHotels', refineHotelsComponent);

export default refineHotelsModule;
