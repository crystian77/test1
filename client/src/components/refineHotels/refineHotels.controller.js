class RefineHotelsController {
	/*@ngInject*/
	stars = [];
	selectAll = true;

	constructor($timeout){
		this.services = {
			$timeout
		};
	}
	$onInit(){
		//Horrible, I know, after 10 hours ... at 8am...
		this.stars.push({selected: true, value: 0});
		this.stars.push({selected: true, value: 0});
		this.stars.push({selected: true, value: 0});
		this.stars.push({selected: true, value: 0});
		this.stars.push({selected: true, value: 0});
		
		this.refine();
	}

	refine(){
		let filterObject = {};
		
		if(this.filterName && this.filterName !== ''){filterObject.name = this.filterName;}
		filterObject.stars = this.stars;
		
		this.filter({$event: filterObject});

		//hack feo
		this.services.$timeout(()=>{
			this.price = {
				min: this.validRanges.min,
				max: this.validRanges.max,
				maxSelected: this.validRanges.max,
				minSelected: this.validRanges.min
			};
			
			
			// //Horrible, I know, after 10 hours ... at 8am...
			this.stars[0] = {selected: true, value: this.validRanges.stars[0]};
			this.stars[1] = {selected: true, value: this.validRanges.stars[1]};
			this.stars[2] = {selected: true, value: this.validRanges.stars[2]};
			this.stars[3] = {selected: true, value: this.validRanges.stars[3]};
			this.stars[4] = {selected: true, value: this.validRanges.stars[4]};
		},10);
		
		
	}

	toggleSelectAll(){
		this.selectAll = !this.selectAll;
		this.stars.forEach(star => {star.selected = this.selectAll;});
	}

	toggleStars(star){
		star.selected = !star.selected;

		let selectAll = true;
		this.stars.forEach(star =>{
			if(!star.selected){
				selectAll = false;
				return;
			}
		});
		
		this.selectAll = selectAll;
	}

}

export default RefineHotelsController;
