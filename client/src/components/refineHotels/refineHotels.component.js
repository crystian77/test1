import template from './refineHotels.html';
import controller from './refineHotels.controller';

let refineHotelsComponent = {
	restrict: 'E',
	bindings: {
		hotels: '<',
		validRanges: '<',
		filter: '&'
	},
	template,
	controller,
	controllerAs: 'vm'
};

export default refineHotelsComponent;
