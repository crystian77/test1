import headerModule from './header/header';
import searcherModule from './searcher/searcher';
import breadcrumbModule from './breadcrumb/breadcrumb';
import mapModule from './map/map';
import refineHotelsModule from './refineHotels/refineHotels';
import resultModule from './result/result';
import hotelModule from './hotel/hotel';

let componentsModule = angular.module('componentsModule', [
	headerModule.name,
	searcherModule.name,
	breadcrumbModule.name,
	mapModule.name,
	refineHotelsModule.name,
	resultModule.name,
	hotelModule.name
]);

export default componentsModule;
