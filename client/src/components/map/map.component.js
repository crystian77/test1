import template from './map.html';

let mapComponent = {
	restrict: 'E',
	template
};

export default mapComponent;
