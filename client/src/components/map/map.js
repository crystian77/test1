import mapComponent from './map.component';

let mapModule = angular.module('mapModule', [])
	.component('mapImage', mapComponent);

export default mapModule;
