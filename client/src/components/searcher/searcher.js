import searcherComponent from './searcher.component';

let searcherModule = angular.module('searcherModule', [])
	.component('searcher', searcherComponent);

export default searcherModule;
