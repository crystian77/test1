import template from './searcher.html';

let searcherComponent = {
	restrict: 'E',
	bindings: {
		place: '<'
	},
	template,
	controllerAs: 'vm'
};

export default searcherComponent;
