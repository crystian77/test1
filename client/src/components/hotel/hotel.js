import itemComponent from './hotel.component';

let itemModule = angular.module('itemModule', [])
	.component('item', itemComponent);

export default itemModule;
