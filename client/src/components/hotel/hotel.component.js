import template from './hotel.html';
import controller from './hotel.controller';

let hotelComponent = {
	restrict: 'E',
	bindings: {
		hotel: '<'
	},
	template,
	controller,
	controllerAs: 'vm'
};

export default hotelComponent;
