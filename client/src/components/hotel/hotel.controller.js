class hotelController {
	/*@ngInject*/
	
	config = {};
	currencyRatio = 0;
	
	constructor(ConfigService){
		this.services = {
			ConfigService
		}
	}
	
	$onInit(){
		this.services.ConfigService.config.then(config => {
			this.config = config;
			this.currencyRatio = _.find(config.currencies, {id: 2}).ratio;
		});
	}
	
	resolveServiceType(serviceType){
		let r = _.find(this.config.servicesType, {id: serviceType});
		return r && r.description;
	}
	
	resolveServiceIcons(serviceIcons = []){
		const icons = serviceIcons.map(icon => {
			let r = _.find(this.config.servicesIcon, {id: icon});
			return r && r.description;
		});
		
		return icons;
	}
	
	gotoHotel(id){
		console.log('gotoHotel',id);
	}
	
}

export default hotelController;
