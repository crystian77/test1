import headerComponent from './header.component';

let headerModule = angular.module('headerModule', [])
	.component('headerCustom', headerComponent);

export default headerModule;
