import template from './header.html';

let headerComponent = {
	restrict: 'E',
	template
};

export default headerComponent;
