import resultComponent from './result.component';

let resultModule = angular.module('resultModule', [])
	.component('result', resultComponent);

export default resultModule;
