import template from './result.html';
import controller from './result.controller';

let resultComponent = {
	restrict: 'E',
	bindings: {
		hotels: '<'
	},
	template,
	controller,
	controllerAs: 'vm'
};

export default resultComponent;
