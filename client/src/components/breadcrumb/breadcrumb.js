import breadcrumbComponent from './breadcrumb.component';

let breadcrumbModule = angular.module('breadcrumbModule', [])
	.component('breadcrumb', breadcrumbComponent);

export default breadcrumbModule;
