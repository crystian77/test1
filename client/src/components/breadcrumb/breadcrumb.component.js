import template from './breadcrumb.html';

let breadcrumbComponent = {
	restrict: 'E',
	bindings:{
		breads: '<'
	},
	template,
	controllerAs: 'vm'
};


export default breadcrumbComponent;
