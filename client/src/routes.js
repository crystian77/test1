export default function routes($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: true,
    });

    $routeProvider
        .otherwise('/')
        .when('/', {
            template: '<landing resolve="$resolve"></landing>',
            resolve: {
                config: ConfigService => ConfigService.config
            }
        })
        .when('/search/:place', {
            template: '<search-section resolve="$resolve"></search-section>',
            resolve: {
                config: ConfigService => ConfigService.config,
				place: (ApiService, $route) => {
                	return ApiService.search(_.get($route, 'current.params.place', ''));
                }
            }
        });
}
