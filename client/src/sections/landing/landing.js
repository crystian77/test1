import landingComponent from './landing.component';

let landingModule = angular.module('landingModule', [])
	.component('landing', landingComponent);

export default landingModule;
