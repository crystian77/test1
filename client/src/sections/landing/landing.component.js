import controller from './landing.controller';
import template from './landing.html';

let landingComponent = {
	restrict: 'E',
	bindings:{
		resolve: '<'
	},
	template,
	controller,
	controllerAs: 'vm'
};

export default landingComponent;
