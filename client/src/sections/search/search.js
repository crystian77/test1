import searchComponent from './search.component';

let searchModule = angular.module('searchModule', [])
	.component('searchSection', searchComponent);

export default searchModule;
