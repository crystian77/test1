class SearchController {
	/*@ngInject*/
	place = {};
	steps = [];
	
	constructor(FilterService){
		this.FilterService = FilterService;
	}
	
	$onInit(){
		this.place = _.get(this,'resolve.place', {place:''});
		this.steps = ['home', 'hoteles', this.place.place];
		this.hotelsOriginal = this.place.hotels;//client side
		this.filter({}); //just for init
	}
	
	filter(filter){
		this.hotels = _.filter(this.hotelsOriginal, hotel =>{
			let match = false;
			if(filter.name){
				match = (hotel.name.toLowerCase().indexOf(filter.name.toLowerCase()) !== -1);
			} else {
				match = true; //all included
			}
			
			// debugger
			// if(!(match && filter.stars[hotel.stars-1].selected)){
			// 	match = false;
			// }
			return match;
		});
		
		this.validRanges = this.FilterService.getRangesFromHotels(this.hotels);
		this.hotelsRefined = this.hotels;//ver!
	}
	
}

export default SearchController;
