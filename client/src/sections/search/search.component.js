import controller from './search.controller';
import template from './search.html';

let searchComponent = {
	restrict: 'E',
	bindings:{
		resolve: '<'
	},
	template,
	controller,
	controllerAs: 'vm'
};

export default searchComponent;
