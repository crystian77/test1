import landingModule from './landing/landing';
import searchModule from './search/search';

let sectionsModule = angular.module('sectionsModule', [
	landingModule.name,
	searchModule.name
]);

export default sectionsModule;
