// import 'normalize.css'

import 'lodash';
import angular from 'angular';
import 'angular-route';
import ngAnimate from 'angular-animate';
import ngAria from 'angular-aria';
import ngMaterial from 'angular-material';
import ngSanitize from 'angular-sanitize';
import 'angular-material/angular-material.css';

import routes from './routes';
import servicesModule from './services'
import sectionsModule from './sections'
import componentsModule from './components'

angular.module('app.main', ['ngRoute',
		ngAnimate,
		ngAria,
		ngMaterial,
		ngSanitize,
		servicesModule.name,
		sectionsModule.name,
		componentsModule.name
	])
	.config(routes);

angular.bootstrap(document, ['app.main']);
